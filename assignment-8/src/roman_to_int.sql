--This comment verify that is Samvel Pleshakov's assignment
﻿CREATE OR REPLACE FUNCTION check_roman_digit(
		roman text
) RETURNS INTEGER
AS $$
DECLARE
	roman_numbers text[] := '{M, D, C, L, X, V, I}';
	int_numbers INTEGER[] := '{1000, 500, 100, 50, 10, 5, 1}';
BEGIN
	IF roman = 'M' THEN
		RETURN 1000;
	END IF;
	
	IF roman = 'D' THEN
		RETURN 500;
	END IF;

	IF roman = 'C' THEN
		RETURN 100;
	END IF;
	
	IF roman = 'L' THEN
		RETURN 50;
	END IF;
	
	IF roman = 'X' THEN
		RETURN 10;
	END IF;
	
	IF roman = 'V' THEN
		RETURN 5;
	END IF;
	
	IF roman = 'I' THEN
		RETURN 1;
	END IF;

	RETURN 0;
END;$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION roman_to_int(
		roman text
) RETURNS INTEGER
AS $$
DECLARE
	incoming_digits text[];
	string_pointer INTEGER := 1;
	string_length INTEGER := 0;
	numbers INTEGER[];
	numbers_pointer INTEGER := 1;
	numbers_length INTEGER := 0;
	result INTEGER := 0;
	unexisted_digit INTEGER := 1000001; -- Digit greater than we can parse
	current_digit INTEGER := 0;
	next_digit INTEGER := 0;
	previous_digit INTEGER := unexisted_digit;
	sequence_length INTEGER := 0;
	skip_next INTEGER := 0;
	check_symbols text;
BEGIN
	roman := upper(roman);
	check_symbols := substring(roman from '[^MDCLXVI̲̅]');
	IF check_symbols <> '' THEN
		RAISE EXCEPTION 'Incorrect symbol: %', check_symbols;
	END IF;
	incoming_digits := regexp_split_to_array(roman,'');
	string_length = array_length(incoming_digits, 1);
	WHILE string_pointer <= string_length LOOP
		check_symbols := substring(incoming_digits[string_pointer] from '[MDCLXVI]');
		IF check_symbols <> '' THEN
			current_digit := check_roman_digit(check_symbols);
			string_pointer := string_pointer + 1;
		ELSE
			RAISE EXCEPTION 'Wrong symbol at %', string_pointer;
		END IF;

		check_symbols := substring(incoming_digits[string_pointer] from '[̲̅]');
		IF check_symbols <> '' THEN
			IF current_digit > 1 THEN
				current_digit := current_digit * 1000;
				string_pointer := string_pointer + 1;
			ELSE 
				RAISE EXCEPTION '"I" digit cannot be with overline/underline modifier. Check % position', string_pointer;
			END IF;
		END IF;

		numbers[numbers_pointer] := current_digit;
		numbers_pointer := numbers_pointer + 1;
	END LOOP;

	numbers_pointer := 1;
	numbers_length := array_length(numbers, 1);
	WHILE numbers_pointer <= numbers_length LOOP
		current_digit := numbers[numbers_pointer];
		IF current_digit > previous_digit THEN
			RAISE EXCEPTION 'Incorrect digits order, digit: %', numbers_pointer;
		ELSIF current_digit = previous_digit AND sequence_length >= 3 THEN
			RAISE EXCEPTION 'Incorrect digits sequence, digit: % % % % % ', numbers_pointer, sequence_length, current_digit, previous_digit, numbers;
		ELSIF current_digit < previous_digit THEN
			sequence_length := 0;
		END IF;
		sequence_length := sequence_length + 1;
		IF numbers_pointer < numbers_length THEN
			next_digit := numbers[numbers_pointer + 1];
			IF current_digit < next_digit THEN
				IF sequence_length > 1 THEN
					RAISE EXCEPTION 'Incorrect digits sequence, position: %', numbers_pointer + 1;
				END IF;
				result := result - current_digit + next_digit;
				sequence_length := 0;
				previous_digit := current_digit/2;
				numbers_pointer := numbers_pointer + 2;
				next_digit := 0;
				CONTINUE;
			END IF;
		END IF;
		result := result + current_digit;
		previous_digit := current_digit;
		numbers_pointer := numbers_pointer + 1;
	END LOOP;
	
	return result;
END;$$
LANGUAGE plpgsql;

select * from roman_to_int('M̲D̲C̲L̲X̲V̲I');