--This comment verify that is Samvel Pleshakov's assignment
﻿CREATE OR REPLACE FUNCTION r_sub(
		r_1 text, r_2 text
	) RETURNS text
AS $$
DECLARE
	n_1 INTEGER;
	n_2 INTEGER;
BEGIN
	n_1 := roman_to_int(r_1);
	n_2 := roman_to_int(r_2);
	RETURN int_to_roman(n_1 - n_2);
END;$$
LANGUAGE plpgsql;

SELECT * FROM r_sub('ii', 'i');