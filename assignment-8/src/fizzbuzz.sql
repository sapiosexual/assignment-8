--This comment verify that is Samvel Pleshakov's assignment
CREATE OR REPLACE FUNCTION fizzbuzz() RETURNS

AS $$
DECLARE
	iterations INTEGER := 100;
	counter INTEGER := 0;
	pointer INTEGER := 0;
	numbers	INTEGER[];
	fizzbuzz text[];
BEGIN
	LOOP
		EXIT WHEN counter = iterations;
		counter := counter + 1;

		IF counter % 3 = 0 AND counter % 5 = 0 THEN
			fizzbuzz[pointer] := 'fizzbuzz';
		
		ELSIF counter % 3 = 0 THEN
			fizzbuzz[pointer] := 'fizz';

		ELSIF counter % 5 = 0 THEN
			fizzbuzz[pointer] := 'buzz';
		END IF;

		IF counter % 3 = 0 OR counter % 5 = 0 THEN
			numbers[pointer] := counter;
			pointer := pointer + 1;
		END IF;
	END LOOP;
	RETURN QUERY SELECT numbers[i], fizzbuzz[i] 
	from generate_subscripts(numbers, 1) g(i);
END;$$
LANGUAGE plpgsql;

SELECT * FROM fizzbuzz();