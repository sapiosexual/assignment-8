--This comment verify that is Samvel Pleshakov's assignment
﻿CREATE OR REPLACE FUNCTION int_to_roman(
		int_number INTEGER
	) RETURNS text
AS $$
DECLARE
	roman_digits text[][] := '{{M̲, M̲M̲, M̲M̲M̲, NULL, NULL, NULL, NULL, NULL, NULL}, 
				   {C̲, C̲C̲, C̲C̲C̲, C̲D̲, D̲, D̲C̲, D̲C̲C̲, D̲C̲C̲C̲, C̲M̲},
				   {X̲, X̲X̲, X̲X̲X̲, X̲L̲, L̲, L̲X̲, L̲X̲X̲, L̲X̲X̲X̲, X̲C̲},
				   {M, MM, MMM, MV̲, V̲, V̲M, V̲MM, V̲MMM, MX̲},
				   {C, CC, CCC, CD, D, DC, DCC, DCCC, CM},
				   {X, XX, XXX, XL, L, LX, LXX, LXXX, XC},
				   {I, II, III, IV, V, VI, VII, VIII, IX}}';
	weights INTEGER[] := '{1, 2, 3, 4, 5, 6, 7, 8, 9}';
	result text := '';
	weight INTEGER := 1000000;
	digit INTEGER := 0;
BEGIN
	IF int_number > 3000000 THEN
		RAISE EXCEPTION 'Sorry, guys, number is too large';
	END IF;

	FOR counter IN 1..array_length(roman_digits, 1) LOOP
		digit := int_number / weight;
		result := concat(result, roman_digits[counter][digit]);
		int_number := int_number - digit * weight;
		weight := weight / 10;
	END LOOP;

	RETURN result;
END;$$
LANGUAGE plpgsql;

SELECT * FROM int_to_roman(123456);
